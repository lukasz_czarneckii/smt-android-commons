package com.smtsoftware.androidcommon.activity;

import com.smtsoftware.androidcommon.R;
import com.smtsoftware.common.imageloader.ImageLoader;
import com.smtsoftware.common.android.imageloader.impl.FrescoImageLoader;
import com.smtsoftware.common.android.imageloader.impl.PicassoImageLoader;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class ImageLoaderActivity extends AppCompatActivity {

    private ImageLoader mImageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //In case of Fresco this has to be called before setContentView
        mImageLoader = new PicassoImageLoader(this);

        setContentView(R.layout.activity_image_loader);
    }

    @Override
    protected void onResume() {
        super.onResume();

        testImageLoader();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_loader, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void testImageLoader() {
        final String sampleImageUrl = "http://image3.redbull.com/rbcom/010/2015-04-05/1331715252374_2/0010/1/1400/933/2/red-bull-show-run-2015-india-hyderabad.jpg";
        final ImageView imageView = (ImageView) findViewById(R.id.sample_image);
        mImageLoader.loadImage(sampleImageUrl, imageView);
    }
}
