package com.smtsoftware.common.android.imageloader;

import android.widget.ImageView;

/**
 * Abstract interface for asynchronous image loader.
 *
 * @author lukasz.czarnecki
 */
public interface ImageLoader {

    /**
     * Performs image pre-fetching: downloads image and store it in cache without displaying.
     *
     * @param url url of the image
     */
    void prefetchImage(String url);

    /**
     * Performs image pre-fetching: downloads image and store it in cache without displaying.
     *
     * @param url       url of the image
     * @param imageSize desired size class of an image
     */
    void prefetchImage(String url, ImageSize imageSize);

    /**
     * Downloads and loads image into specified image view.
     *
     * @param url       url of the image
     * @param imageView view in which image will be loaded
     */
    void loadImage(String url, ImageView imageView);

    /**
     * Downloads and loads image into specified image view.
     *
     * @param url       url of the image
     * @param imageView view in which image will be loaded
     * @param imageSize desired size class of an image
     */
    void loadImage(String url, ImageView imageView, ImageSize imageSize);
}
