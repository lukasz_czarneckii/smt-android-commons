package com.smtsoftware.common.android.imageloader;

/**
 * Size classes for remote images. Each @{@link ImageLoader} implementation may handle each class
 * in different way - see relevant implementation's implementation.
 *
 * @author lukasz.czarnecki
 * @see com.smtsoftware.common.android.imageloader.impl.PicassoImageLoader
 * @see com.smtsoftware.common.android.imageloader.impl.FrescoImageLoader
 */
public enum ImageSize {
    SMALL,
    NORMAL,
    LARGE
}
