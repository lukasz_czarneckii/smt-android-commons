package com.smtsoftware.common.android.imageloader.impl;

import android.app.ActivityManager;
import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.internal.Supplier;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.cache.DefaultBitmapMemoryCacheParamsSupplier;
import com.facebook.imagepipeline.cache.DefaultEncodedMemoryCacheParamsSupplier;
import com.facebook.imagepipeline.cache.MemoryCacheParams;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.smtsoftware.common.android.imageloader.ImageLoader;
import com.smtsoftware.common.android.imageloader.ImageSize;
import com.smtsoftware.common.android.R;

import java.io.File;

/**
 * <p>Implementation of image loader, based on Facebook's Fresco library.</p>
 * <p>NOTE: to use it, Fresco's @{link com.facebook.drawee.view.SimpleDraweeView} has to be used
 * in layouts instead of native ImageViews.</p>
 * <p>Supports:</p>
 * <ul>
 * <li>image pre-fetching</li>
 * <li>configurable memory cache (separate for small and large images)</li>
 * <li>configurable disc cache (separate for compressed and uncompressed bitmaps)</li>
 * </ul>
 * <p>Supports:</p>
 * <ul>
 * <li>image resize via @{ImageSize} class - according to documentation this operation slows down
 * entire pipeline; hardware-accelerated scaling should be used instead</li>
 * </ul>
 * <p>Project's website: http://frescolib.org/</p>
 *
 * @author lukasz.czarnecki
 */
public class FrescoImageLoader implements ImageLoader {

    private final Context mContext;

    /**
     * Default constructor
     *
     * @param context context
     */
    public FrescoImageLoader(final Context context) {
        mContext = context;
        rebuildFresco();
    }

    @Override
    public void prefetchImage(final String url) {
        prefetchImage(url, ImageSize.LARGE);
    }

    @Override
    public void prefetchImage(final String url, final ImageSize imageSize) {
        final ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final Uri uri = Uri.parse(url);
        imagePipeline.prefetchToDiskCache(ImageRequest.fromUri(uri), null);
    }

    @Override
    public void loadImage(final String url, final ImageView imageView) {
        loadImage(url, imageView, ImageSize.LARGE);
    }

    @Override
    public void loadImage(final String url, final ImageView imageView, final ImageSize imageSize) {
        if (!(imageView instanceof SimpleDraweeView)) {
            throw new ClassCastException("To use Fresco, you need to use " +
                    "com.facebook.drawee.view.SimpleDraweeView instead of ImageView");
        }

        final Uri uri = Uri.parse(url);
        final ImageRequest request;
        final SimpleDraweeView draweeView = (SimpleDraweeView) imageView;

        // Resizing with Fresco is not implemented for now
        switch (imageSize) {
            case SMALL:
                request = ImageRequestBuilder.newBuilderWithSource(uri)
                        .setImageType(ImageRequest.ImageType.SMALL)
                        .build();
                break;
            default:
                request = ImageRequest.fromUri(uri);
                break;
        }
        final AbstractDraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(draweeView.getController())
                .setImageRequest(request)
                .build();
        draweeView.setController(controller);
    }

    private void rebuildFresco() {
        final ImagePipelineConfig config = ImagePipelineConfig.newBuilder(mContext)
                .setBitmapMemoryCacheParamsSupplier(getBitmapMemoryCacheConfig())
                .setEncodedMemoryCacheParamsSupplier(getEncodedMemoryCacheConfig())
                .setMainDiskCacheConfig(getDiskCacheConfig())
                .setSmallImageDiskCacheConfig(getSmallImageDiskCacheConfig())
                .setIsPrefetchEnabledSupplier(new Supplier<Boolean>() {
                    @Override
                    public Boolean get() {
                        return true;
                    }
                })
                .build();

        Fresco.initialize(mContext, config);
    }

    private DiskCacheConfig getSmallImageDiskCacheConfig() {
        final int smallDiskCacheSize =
                mContext.getResources().getInteger(R.integer.fresco_image_loader_small_image_disk_cache_size);

        final String diskCacheDirPath = mContext.getCacheDir().getAbsolutePath();

        final String diskCacheDirName =
                mContext.getResources().getString(R.string.fresco_image_loader_small_image_disk_cache_dir_name);

        // Other params can be found here:
        // http://frescolib.org/javadoc/reference/com/facebook/cache/disk/DiskCacheConfig.Builder.html
        return DiskCacheConfig.newBuilder()
                .setMaxCacheSize(smallDiskCacheSize)
                .setBaseDirectoryName(diskCacheDirName)
                .setBaseDirectoryPathSupplier(new Supplier<File>() {
                    @Override
                    public File get() {
                        return new File(diskCacheDirPath);
                    }
                })
                .build();
    }

    private DiskCacheConfig getDiskCacheConfig() {
        final int diskCacheSize =
                mContext.getResources().getInteger(R.integer.fresco_image_loader_disk_cache_size);

        final String diskCacheDirPath = mContext.getCacheDir().getAbsolutePath();

        final String diskCacheDirName =
                mContext.getResources().getString(R.string.fresco_image_loader_disk_cache_dir_name);

        // Other params can be found here:
        // http://frescolib.org/javadoc/reference/com/facebook/cache/disk/DiskCacheConfig.Builder.html
        return DiskCacheConfig.newBuilder()
                .setMaxCacheSize(diskCacheSize)
                .setBaseDirectoryName(diskCacheDirName)
                .setBaseDirectoryPathSupplier(new Supplier<File>() {
                    @Override
                    public File get() {
                        return new File(diskCacheDirPath);
                    }
                })
                .build();
    }

    private Supplier<MemoryCacheParams> getEncodedMemoryCacheConfig() {
        final int memoryCacheSize =
                mContext.getResources().getInteger(R.integer.fresco_image_loader_encoded_mem_cache_size);

        final MemoryCacheParams defaultParams =
                new DefaultEncodedMemoryCacheParamsSupplier().get();

        return new Supplier<MemoryCacheParams>() {
            @Override
            public MemoryCacheParams get() {
                return new MemoryCacheParams(
                        memoryCacheSize,
                        defaultParams.maxCacheEntries,
                        defaultParams.maxEvictionQueueSize,
                        defaultParams.maxEvictionQueueEntries,
                        defaultParams.maxCacheEntrySize);
            }
        };
    }

    private Supplier<MemoryCacheParams> getBitmapMemoryCacheConfig() {
        final int memoryCacheSize =
                mContext.getResources().getInteger(R.integer.fresco_image_loader_bitmap_mem_cache_size);

        final ActivityManager activityManager =
                (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);

        final MemoryCacheParams defaultParams =
                new DefaultBitmapMemoryCacheParamsSupplier(activityManager).get();

        return new Supplier<MemoryCacheParams>() {
            @Override
            public MemoryCacheParams get() {
                return new MemoryCacheParams(
                        memoryCacheSize,
                        defaultParams.maxCacheEntries,
                        defaultParams.maxEvictionQueueSize,
                        defaultParams.maxEvictionQueueEntries,
                        defaultParams.maxCacheEntrySize);
            }
        };
    }
}
