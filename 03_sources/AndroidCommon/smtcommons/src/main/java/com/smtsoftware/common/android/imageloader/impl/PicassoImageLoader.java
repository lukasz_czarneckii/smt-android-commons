package com.smtsoftware.common.android.imageloader.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.smtsoftware.common.android.imageloader.ImageLoader;
import com.smtsoftware.common.android.imageloader.ImageSize;
import com.smtsoftware.common.android.R;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * <p>Implementation of image loader, based on SquareUp's Picasso library.</p>
 * <p>Supports:</p>
 * <ul>
 * <li>image pre-fetching</li>
 * <li>configurable memory cache</li>
 * <li>image resize via @{ImageSize} class</li>
 * </ul>
 * <p>Supports:</p>
 * <ul>
 * <li>configurable disc cache - disc cache is handled by OkHttpClient</li>
 * </ul>
 * <p>Project's website: http://square.github.io/picasso/</p>
 *
 * @author lukasz.czarnecki
 */
public class PicassoImageLoader implements ImageLoader {

    private final Context mContext;

    /**
     * Default constructor
     *
     * @param context context for Picasso
     */
    public PicassoImageLoader(final Context context) {
        mContext = context;
        rebuildPicasso();
    }

    @Override
    public void prefetchImage(final String url) {
        prefetchImage(url, ImageSize.LARGE);
    }

    @Override
    public void prefetchImage(final String url, final ImageSize imageSize) {
        final RequestCreator requestCreator = getRequestCreator(url, imageSize);
        requestCreator.fetch();
    }

    @Override
    public void loadImage(final String url, final ImageView imageView) {
        loadImage(url, imageView, ImageSize.LARGE);
    }

    @Override
    public void loadImage(final String url, final ImageView imageView, final ImageSize imageSize) {
        final RequestCreator requestCreator = getRequestCreator(url, imageSize);
        requestCreator.into(imageView);
    }

    @NonNull
    private RequestCreator getRequestCreator(final String url, final ImageSize imageSize) {
        final RequestCreator requestCreator = Picasso.with(mContext).load(url);
        switch (imageSize) {
            case SMALL:
                requestCreator.resizeDimen(
                        R.dimen.image_loader_small_image_size,
                        R.dimen.image_loader_small_image_size);
                break;
            case NORMAL:
                requestCreator.resizeDimen(
                        R.dimen.image_loader_normal_image_size,
                        R.dimen.image_loader_normal_image_size);
                break;
            default:
                break;
        }
        return requestCreator;
    }

    private void rebuildPicasso() {
        final int memoryCacheSize =
                mContext.getResources().getInteger(R.integer.image_loader_memory_cache_size);
        final OkHttpClient client = new OkHttpClient();
        final Downloader downloader = new OkHttpDownloader(client);
        final Picasso picassoInstance = new Picasso.Builder(mContext)
                .memoryCache(new LruCache(memoryCacheSize))
                .downloader(downloader)
                .build();
        try {
            Picasso.setSingletonInstance(picassoInstance);
        } catch (final IllegalStateException e) {
            //Throw when we try to override Picasso's singleton instance.
            //This helper should be singleton too - then we will avoid this error.
        }
    }
}
